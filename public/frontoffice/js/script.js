$(document).ready(function() {
    $('.kl-date-picker').datepicker({
        language: 'fr',
        autoclose: true
    });
    $('.timepicker').timepicker({
      showInputs: false
    });
    getFoodHome();
    getFoodMenu();

    $('.kl-btn-valid-reservation').click(function(){
        saveCustomer();
    });

    /*Submit book now*/
    $('.kl-submit-book-now').on('click', function(){
        //alert('ici');
        saveCustomer();
    });
}); 

/*Get all food*/
function getFoodHome(current_page = 1) {
    $('.kl-spinner-tips').show();
    $.ajax({
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        url: 'http://127.0.0.1:8000/api/food',
        cache: false,
        success: function (response) {
            if(response != null){
                let index_value ;
                let i = 0;
                for(index_value in response){
                    if(i > 3) return true;
                    i++;
                    if(response.hasOwnProperty(index_value))
                        var food = response[index_value];
                    $('.kl-food-result').append('<div class="col-lg-6 kl-bloc-price"><div class="kl-price">'+
                            '<span>$ '+response[index_value].price+'</span></div>'+
                            '<div class="kl-text-menu"><h5>'+response[index_value].title+'</h5>'+
                            '<p>'+response[index_value].description+'</p></div>'+
                        '</div>');

                }
            } else $('.kl-food-result').append('<span>No records</span>');
        },
        error: function () {
            $('.kl-spinner-tips').hide();            
            //alert(exception_error);
        }
    });
}

/*Get all food*/
function getFoodMenu() {
    $('.kl-spinner-tips').show();
    $.ajax({
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        url: 'http://127.0.0.1:8000/api/food',
        cache: false,
        success: function (response) {
            let index_value ;
            let i = 0, j = 0, k = 0;
            for(index_value in response){
                if(response.hasOwnProperty(index_value))
                    var food = response[index_value];
                if(response[index_value].type == '/api/food_types/1'){
                    if(response[index_value] != null){
                        if(i > 4) return;
                        i++;
                        $('.kl-food-starters').append('<div class="kl-menu-price mb-3 mt-3">'+
                            '<div class="kl-price">'+
                                '<span>$'+food.price+'</span>'+
                            '</div>'+
                            '<div class="kl-text-menu">'+
                                '<h5>'+food.title+'</h5>'+
                                '<p>'+food.description+'</p>'+
                            '</div>'+
                        '</div>');
                    }
                    else $('.kl-food-starters').append('<span>No records</span>');
                }
                if(response[index_value].type == '/api/food_types/2'){
                    if(response[index_value] != null){
                        if(j > 4) return true;
                        j++;
                        $('.kl-food-mains').append('<div class="kl-menu-price mb-3 mt-3">'+
                            '<div class="kl-price">'+
                                '<span>$'+food.price+'</span>'+
                            '</div>'+
                            '<div class="kl-text-menu">'+
                                '<h5>'+food.title+'</h5>'+
                                '<p>'+food.description+'</p>'+
                            '</div>'+
                        '</div>');
                    }
                    else $('.kl-food-mains').append('<span>No records</span>');
                }
                if(response[index_value].type == '/api/food_types/3'){
                    if(response[index_value] != null){
                        if(k > 4) return true;
                        k++;
                        $('.kl-food-pastries').append('<div class="kl-menu-price mb-3 mt-3">'+
                            '<div class="kl-price">'+
                                '<span>$'+food.price+'</span>'+
                            '</div>'+
                            '<div class="kl-text-menu">'+
                                '<h5>'+food.title+'</h5>'+
                                '<p>'+food.description+'</p>'+
                            '</div>'+
                        '</div>');
                    }
                    else $('.kl-food-pastries').append('<span>No records</span>');
                }
            }
        },
        error: function () {
            $('.kl-spinner-tips').hide();            
            //alert(exception_error);
        }
    });
}

function saveCustomer(){
    $('.kl-submit-book-now').attr('disabled', true);
    $('.kl-btn-valid-reservation').attr('disabled', true);
    var customer = new Object();
    customer.name = $('#id-name').val();
    customer.firstname = $('#id-first-name').val();
    customer.email = $('#id-email').val();
    customer.phone = $('#id-phone').val();
    var is_valid = $('#id-name').val() != "" && $('#id-first-name').val() != "" && $('#id-email').val() != ""  && $('#id-phone').val() != "" &&  $('#id-book-now').val() !="" &&  $('#id-number-person').val() != "" && $('#id-book-time').val() != "";
    if(is_valid){
        $.ajax({
            type: 'POST',
            url: 'http://127.0.0.1:8000/api/customers',
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(customer),
            success: function (response, texStatus, xhr) {
                saveReservation(response.id)
            },
            error: function () {
                $('.kl-spinner-tips').hide();            
                //alert(exception_error);
            }
        });
    } else {
        $('.kl-submit-book-now').removeAttr('disabled');
        $('.kl-btn-valid-reservation').removeAttr('disabled');
        setTimeout(function () {
            alert('All input is required');
        },100);
    }
}

function saveReservation(value){
    var reservation = new Object();
    reservation.customer = '/api/customers/'+value;
    reservation.bookAt = $('#id-book-now').val();
    reservation.numberPerson = $('#id-number-person').val();
    reservation.code = $('#id-book-time').val();
    reservation.isAccepted = false;
    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:8000/api/reservations',
        dataType: 'json',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        data: JSON.stringify(reservation),
        success: function (response, texStatus, xhr) {
            setTimeout(function () {
                alert('Reservation save');
            },500);
            $('.kl-submit-book-now').removeAttr('disabled');
            $('.kl-btn-valid-reservation').removeAttr('disabled');
            $('#id-name').val("");
            $('#id-first-name').val("");
            $('#id-email').val("");
            $('#id-phone').val("");
            $('#id-book-now').val("");
            $('#id-book-time').val("6:00 PM");
            $('#id-number-person').val("");
            $('#id-modal-book-now').modal('hide');
        },
        error: function () {
            $('.kl-spinner-tips').hide();            
            //alert(exception_error);
        }
    });
}
