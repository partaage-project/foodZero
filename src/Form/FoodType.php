<?php

namespace App\Form;

use App\Entity\Food;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class FoodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('price')
            ->add('type', EntityType::class, array(
                    'label'         => 'Type',
                    'class'         => 'App\Entity\FoodType',
                    'query_builder' => function (EntityRepository $_er) {
                        $_query_builder = $_er->createQueryBuilder('tp')
                            ->where('tp.id IS NOT NULL');

                        return $_query_builder;
                    },
                    'choice_label'  => 'name',
                    'multiple'      => false,
                    'expanded'      => false,
                    'placeholder'   => 'Choose',
                    'empty_data'    => null,
                    'attr'          => [
                        'required' => true,
                        'class'    => 'kl-select2'
                    ]
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Food::class,
        ]);
    }
}
