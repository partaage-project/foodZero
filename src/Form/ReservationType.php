<?php

namespace App\Form;

use App\Entity\Reservation;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('numberPerson', IntegerType::class, array(
                'label'    => "Number person",
                'required' => true
            ))
            ->add('code', TextType::class, array(
                    'label'         => 'Time',
                    'required'      => true,
                    'attr'   => [
                        'class' => 'timepicker'
                    ]
                )
            )
            ->add('bookAt', DateTimeType::class, array(
                'label'  => 'Date',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'html5' => false,
                'required' => false,
                'attr'   => [
                    'placeholder' => 'Entrer une date',
                    'autocomplete' => 'off',
                    'class' => 'kl-date-picker'
                ]
            ))
            ->add('isAccepted', CheckboxType::class, array(
                    'label'         => 'Accept ?',
                    'required'      => false
                )
            )
            ->add('customer', EntityType::class, array(
                    'label'         => 'Customer',
                    'class'         => 'App\Entity\Customer',
                    'query_builder' => function (EntityRepository $_er) {
                        $_query_builder = $_er->createQueryBuilder('cst')
                            ->where('cst.id IS NOT NULL')
                            ->orderBy('cst.id', 'DESC');

                        return $_query_builder;
                    },
                    'choice_label'  => 'name',
                    'multiple'      => false,
                    'expanded'      => false,
                    'placeholder'   => 'Choose',
                    'empty_data'    => null,
                    'attr'          => [
                        'required' => true,
                        'class'    => 'kl-select2'
                    ]
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
