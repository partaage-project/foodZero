<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\AcceptReservationAction;
use App\Controller\DeclineReservationAction;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get",
 *      "put",
 *      "delete",
 *      "accept_reservation"={
 *          "method"="GET",
 *          "path"="/reservations/{id}/accept",
 *          "controller"=AcceptReservationAction::class,
 * },
 * "decline_reservation"={
 *          "method"="GET",
 *          "path"="/reservations/{id}/decline",
 *          "controller"=DeclineReservationAction::class,
 * }
 * },
 *  collectionOperations={
 *      "get",
 *      "post"
 * }
 * 
 * )
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $bookAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAccepted;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="reservations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $customer;

    /**
     * @ORM\Column(type="string")
     */
    private $numberPerson;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getBookAt(): ?\DateTime
    {
        return $this->bookAt;
    }

    public function setBookAt(\DateTime $bookAt): self
    {
        $this->bookAt = $bookAt;

        return $this;
    }

    public function isIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(bool $isAccepted): self
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getNumberPerson(): ?string
    {
        return $this->numberPerson;
    }

    public function setNumberPerson(string $numberPerson): self
    {
        $this->numberPerson = $numberPerson;

        return $this;
    }
}
