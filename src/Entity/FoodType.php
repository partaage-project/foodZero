<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FoodTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Controller\CreateFoodTypePictureAction;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *              "normalization_context"={"groups"={"food_type:read"}},
 *              "denormalization_context"={"groups"={"food_type:post:write"}},
 *     },
 *      "delete"={
 *              "normalization_context"={"groups"={"food_type:read"}},
 *              "denormalization_context"={"groups"={"food_type:post:write"}},
 *     },
 *      "put"={
 *              "normalization_context"={"groups"={"food_type:read"}},
 *              "denormalization_context"={"groups"={"food_type:post:write"}},
 *     },
 *      "image"={
 *             "controller"=CreateFoodTypePictureAction::class,
 *             "path"="/food/{id}/image",
 *             "normalization_context"={"groups"={"food_type:read"}},
 *             "method"="POST",
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "food_type_picture_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *      }
 *  },
 * 
 *  collectionOperations={
 *            "post"={
 *                "normalization_context"={"groups"={"food_type:read"}},
 *                "denormalization_context"={"groups"={"food_type:post:write"}},
 *       },
 *            "get"={
 *                "normalization_context"={"groups"={"food_type:read"}},
 *       }
 *  }
 * 
 * 
 * )
 * @ORM\Entity(repositoryClass=FoodTypeRepository::class)
 * @Vich\Uploadable()
 */
class FoodType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"food_type:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"food_type:read", "food_type:post:write"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Food::class, mappedBy="type", orphanRemoval=true)
     */
    private $food;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Groups({"food_type:read"})
     */
    private $updatedAt;

    /**
     * @var string|null
     *
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({"food_type_picture_create"})
     * @Groups({"food_type:read"})
     */
    public $contentUrl;

    /**
     * @var File|null
     *
     * @Assert\NotNull(groups={"food_type_picture_create"})
     * @Vich\UploadableField(mapping="typefood", fileNameProperty="filePath")
     */
    public $file;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    public $filePath;

    public function __construct()
    {
        $this->food = new ArrayCollection();
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     */
    public function setFile(?File $file): FoodType
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string|null $filePath
     */
    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Food>
     */
    public function getFood(): Collection
    {
        return $this->food;
    }

    public function addFood(Food $food): self
    {
        if (!$this->food->contains($food)) {
            $this->food[] = $food;
            $food->setType($this);
        }

        return $this;
    }

    public function removeFood(Food $food): self
    {
        if ($this->food->removeElement($food)) {
            // set the owning side to null (unless already changed)
            if ($food->getType() === $this) {
                $food->setType(null);
            }
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
