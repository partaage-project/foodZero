<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\FoodRepository;

class DefaultController extends AbstractController
{
    /**
     * @Route("/home", name="app_default")
     */
    public function index(): Response
    {
        return $this->render('frontoffice/default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/contact", name="app_contact")
     */
    public function contact(): Response
    {
        return $this->render('frontoffice/default/contact.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/menu-food", name="app_menu_food")
     */
    public function menuFood(FoodRepository $foodRepository): Response
    {
        $foods  = $foodRepository->findAll();
        return $this->render('frontoffice/default/menu_food.html.twig', [
            'foods' => $foods,
        ]);
    }
}
