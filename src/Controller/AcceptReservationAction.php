<?php


namespace App\Controller;


use App\Entity\Reservation;
use Symfony\Component\HttpFoundation\Request;

class AcceptReservationAction
{
    public function __invoke(Request $request): Reservation
    {
        $reservation = $request->attributes->get('data');
        $reservation->setIsAccepted(true);
        return $reservation;
    }
}