<?php


namespace App\Controller;

use App\Entity\FoodType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class CreateFoodTypePictureAction
{
    public function __invoke(Request $request): FoodType
    {
        $type = $request->attributes->get('data');

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $type->setFile($request->files->get('file'));
        $type->setUpdatedAt(new \DateTimeImmutable());
        return $type;
    }
}