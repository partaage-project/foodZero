## FoodZero
Projet : FoodZero TEST

***
PHP 7.4.1 ,
MySQL 8.0.21
***



## Dépendances
Les dépendances Php sont installées avec https://getcomposer.org/
Les dépendances js sont installées avec https://yarnpkg.com/

- run composer install
- run symfony serve 


## Configuration database
Créer un fichier .env.local et ajouter votre configuration de connexion à la base de données 

DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name" (food_zero_test)

Puis exécuter la commande pour créer la base

php  bin/console doctrine:database:create

ou importer la base de données (Fichier --> food_zero_test.sql)



##Exécuter les migrations

php  bin/console doctrine:migrations:migrate

puis, executer ce patch dans SQl de php//MyAdmin 
----> INSERT INTO `food_type` (`id`, `name`, `updated_at`, `file_path`) VALUES (1, 'Starters', NULL, NULL), (2, 'Mains', NULL, NULL), (3, 'Pastries & Drinks', NULL, NULL);




##Lancer le build en temps réel de yarn

Lancer le serveur local

php  -S localhost:8000 -t public

ou 

symfony serve 



## swagger pour les apis créés
http://127.0.0.1:8000/api



## Collection Postman 
--> foodZero.postman_collection.json



## click at menu burger at the header (to link into menu/contact/backoffice/home)
